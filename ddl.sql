-- bookshop.a_file definition

-- Drop table

-- DROP TABLE bookshop.a_file;

CREATE TABLE bookshop.a_file (
	afile_id serial NOT NULL DEFAULT nextval('bookshop.a_file_afile_id_seq'::regclass),
	"content" bytea NOT NULL,
	CONSTRAINT a_file_pkey PRIMARY KEY (afile_id)
);


-- bookshop.genre definition

-- Drop table

-- DROP TABLE bookshop.genre;

CREATE TABLE bookshop.genre (
	genres_id serial NOT NULL DEFAULT nextval('bookshop.genre_genres_id_seq'::regclass),
	"name" varchar(30) NOT NULL,
	CONSTRAINT genre_name_key UNIQUE (name),
	CONSTRAINT genre_pkey PRIMARY KEY (genres_id)
);


-- bookshop.priorities definition

-- Drop table

-- DROP TABLE bookshop.priorities;

CREATE TABLE bookshop.priorities (
	level_id serial NOT NULL DEFAULT nextval('bookshop.priorities_level_id_seq'::regclass),
	p_level varchar(10) NOT NULL,
	CONSTRAINT priorities_pkey PRIMARY KEY (level_id)
);


-- bookshop.status definition

-- Drop table

-- DROP TABLE bookshop.status;

CREATE TABLE bookshop.status (
	status_id serial NOT NULL DEFAULT nextval('bookshop.status_status_id_seq'::regclass),
	status_name varchar(20) NULL,
	CONSTRAINT status_pkey PRIMARY KEY (status_id)
);


-- bookshop.story_type definition

-- Drop table

-- DROP TABLE bookshop.story_type;

CREATE TABLE bookshop.story_type (
	type_id serial NOT NULL DEFAULT nextval('bookshop.story_type_type_id_seq'::regclass),
	type_name varchar(20) NOT NULL,
	CONSTRAINT story_type_pkey PRIMARY KEY (type_id),
	CONSTRAINT story_type_type_name_key UNIQUE (type_name)
);


-- bookshop.usr_role definition

-- Drop table

-- DROP TABLE bookshop.usr_role;

CREATE TABLE bookshop.usr_role (
	role_id serial NOT NULL DEFAULT nextval('bookshop.usr_role_role_id_seq'::regclass),
	"name" varchar(30) NOT NULL,
	CONSTRAINT usr_role_name_key UNIQUE (name),
	CONSTRAINT usr_role_pkey PRIMARY KEY (role_id)
);


-- bookshop.committee definition

-- Drop table

-- DROP TABLE bookshop.committee;

CREATE TABLE bookshop.committee (
	committee_id serial NOT NULL DEFAULT nextval('bookshop.committee_committee_id_seq'::regclass),
	"name" varchar(15) NOT NULL,
	com_genre int4 NULL,
	CONSTRAINT committee_name_key UNIQUE (name),
	CONSTRAINT committee_pkey PRIMARY KEY (committee_id),
	CONSTRAINT committee_com_genre_fkey FOREIGN KEY (com_genre) REFERENCES bookshop.genre(genres_id)
);


-- bookshop.usr definition

-- Drop table

-- DROP TABLE bookshop.usr;

CREATE TABLE bookshop.usr (
	usr_id serial NOT NULL DEFAULT nextval('bookshop.usr_usr_id_seq'::regclass),
	usrname varchar(30) NOT NULL,
	passwd varchar(30) NOT NULL,
	usr_role_id int4 NULL,
	points int4 NULL,
	CONSTRAINT usr_check CHECK ((points <= 100)),
	CONSTRAINT usr_pkey PRIMARY KEY (usr_id),
	CONSTRAINT usr_usrname_key UNIQUE (usrname),
	CONSTRAINT usr_usr_role_id_fkey FOREIGN KEY (usr_role_id) REFERENCES bookshop.usr_role(role_id)
);


-- bookshop.usr_committee definition

-- Drop table

-- DROP TABLE bookshop.usr_committee;

CREATE TABLE bookshop.usr_committee (
	committee_id int4 NULL,
	usr_id int4 NULL,
	CONSTRAINT usr_committee_committee_id_fkey FOREIGN KEY (committee_id) REFERENCES bookshop.committee(committee_id),
	CONSTRAINT usr_committee_usr_id_fkey FOREIGN KEY (usr_id) REFERENCES bookshop.usr(usr_id)
);


-- bookshop.pitch definition

-- Drop table

-- DROP TABLE bookshop.pitch;

CREATE TABLE bookshop.pitch (
	pitch_id int4 NOT NULL DEFAULT nextval('bookshop.my_serial'::regclass),
	title varchar(40) NOT NULL,
	stype_id int4 NULL,
	priority int4 NULL,
	status_id int4 NULL,
	genre_id int4 NULL,
	description varchar(60) NULL,
	authinfo varchar(60) NOT NULL,
	genreapproval varchar NULL,
	assistantapproval varchar NULL,
	author_id int4 NOT NULL,
	suggestion varchar NULL,
	CONSTRAINT pitch_pkey PRIMARY KEY (pitch_id),
	CONSTRAINT pitch_title_key UNIQUE (title),
	CONSTRAINT pitch_fk FOREIGN KEY (author_id) REFERENCES bookshop.usr(usr_id),
	CONSTRAINT pitch_genre_id_fkey FOREIGN KEY (genre_id) REFERENCES bookshop.genre(genres_id),
	CONSTRAINT pitch_priority_fkey FOREIGN KEY (priority) REFERENCES bookshop.priorities(level_id),
	CONSTRAINT pitch_status_id_fkey FOREIGN KEY (status_id) REFERENCES bookshop.status(status_id),
	CONSTRAINT pitch_stype_id_fkey FOREIGN KEY (stype_id) REFERENCES bookshop.story_type(type_id)
);


-- bookshop.pitch_file definition

-- Drop table

-- DROP TABLE bookshop.pitch_file;

CREATE TABLE bookshop.pitch_file (
	pitch int4 NULL,
	file_id int4 NULL,
	CONSTRAINT pitch_file_file_id_fkey FOREIGN KEY (file_id) REFERENCES bookshop.a_file(afile_id),
	CONSTRAINT pitch_file_pitch_fkey FOREIGN KEY (pitch) REFERENCES bookshop.pitch(pitch_id)
);


-- bookshop.suggestion definition

-- Drop table

-- DROP TABLE bookshop.suggestion;

CREATE TABLE bookshop.suggestion (
	suggestion_id serial NOT NULL DEFAULT nextval('bookshop.suggestion_suggestion_id_seq'::regclass),
	pitch_id int4 NOT NULL,
	contents varchar NULL,
	CONSTRAINT suggestion_pk PRIMARY KEY (suggestion_id),
	CONSTRAINT suggestion_fk FOREIGN KEY (pitch_id) REFERENCES bookshop.pitch(pitch_id)
);


-- bookshop.drafts definition

-- Drop table

-- DROP TABLE bookshop.drafts;

CREATE TABLE bookshop.drafts (
	draft_id serial NOT NULL DEFAULT nextval('bookshop.drafts_draft_id_seq'::regclass),
	story_type int4 NULL,
	state varchar NULL,
	contents varchar NULL,
	pitch_id int4 NULL,
	CONSTRAINT drafts_pk PRIMARY KEY (draft_id),
	CONSTRAINT drafts_fk_p_id FOREIGN KEY (pitch_id) REFERENCES bookshop.pitch(pitch_id),
	CONSTRAINT drafts_fk_st FOREIGN KEY (story_type) REFERENCES bookshop.story_type(type_id)
);


-- bookshop.editor_pitch_approvals definition

-- Drop table

-- DROP TABLE bookshop.editor_pitch_approvals;

CREATE TABLE bookshop.editor_pitch_approvals (
	usr_id int4 NULL,
	app_pitch int4 NULL,
	editor_genre int4 NULL,
	approval_id serial NOT NULL DEFAULT nextval('bookshop.editor_pitch_approvals_approval_id_seq'::regclass),
	suggestion varchar(120) NULL,
	CONSTRAINT editor_pitch_approvals_pk PRIMARY KEY (approval_id),
	CONSTRAINT editor_pitch_approvals_app_pitch_fkey FOREIGN KEY (app_pitch) REFERENCES bookshop.pitch(pitch_id),
	CONSTRAINT editor_pitch_approvals_editor_genre_fkey FOREIGN KEY (editor_genre) REFERENCES bookshop.genre(genres_id),
	CONSTRAINT editor_pitch_approvals_usr_id_fkey FOREIGN KEY (usr_id) REFERENCES bookshop.usr(usr_id)
);


-- bookshop.editor_suggestion definition

-- Drop table

-- DROP TABLE bookshop.editor_suggestion;

CREATE TABLE bookshop.editor_suggestion (
	"content" bytea NOT NULL,
	editor_id int4 NULL,
	author_id int4 NULL,
	pitch_id int4 NOT NULL,
	CONSTRAINT editor_suggestion_author_id_fkey FOREIGN KEY (author_id) REFERENCES bookshop.usr(usr_id),
	CONSTRAINT editor_suggestion_editor_id_fkey FOREIGN KEY (editor_id) REFERENCES bookshop.usr(usr_id),
	CONSTRAINT editor_suggestion_fk FOREIGN KEY (pitch_id) REFERENCES bookshop.pitch(pitch_id)
);
