**Project Description**


This is the Story Pitch Management System, a web application designed to help manage a publishing companies workflow in a distributed manner. Users can access the website, authenticate with a user/pass combination, and then have access to read and write pitches which can be approved by management-level Users; these management-level users have their own hierarchy through which they then process these pitches such that the pitch ascends through the responsbility pyramid before a prospective author is given approval for a project.

The backend uses a PostgreSQL database, mapped by a Java/Hibernate API routed with Javalin which is called to by the frontend. The frontend is a combination of HTML and JavaScript, making calls to the backend through the Fetch API.


**Technologies Used**

- PostgreSQL 
- Javalin
- Hibernate


**Features**

- Pitch generation and submission through a frontend
- RESTful API for obtaining information from and writing to the database
- Role-restricted read access for several tiers of approval for pitches
- Feedback to editors and authors

To-do list:

- Implement user restriction to feedback; currently, all authors can see the feedback returned for pitches through a table. Small adjustments necessary to the DB.
- Move DB to AWS

**Getting Started**

From git bash in an empty folder, use the command git clone https://gitlab.com/BCONLON3987/spmp.git to obtain necessary files. Open the folder in an Eclipse workspace. Because we are using Javalin, we only need to run the controller in src/main/java to launch the application. An embedded Jetty server will handle our file hosting. Generate a schema in DBeaver using the ddl.sql file and adjust port settings in BookApp.java as necessary. 

**Usage**

You will have to add your own user to the user database to work with a security measure; the frontend does not desire open registration of authors.
Go to localhost:port to access the main endpoint and login. By default, the port is 8080. Once you are logged in, you will be able to add a pitch from the view pitches tab; creating users at the various management levels will allow you to approve and provide feedback to the pitches up to draft generation.

**Contibutors**

This was an individual project.

**License**

This project uses the following license: <MIT_license>.
